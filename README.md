# Supplementary scripts for Chavez et al., 2021 #

This repository contain script, biom files (in tsv format) and metada used for the manuscript titled "Skin-penetrating parasitic nematodes exhibit life-stage-specific interactions with host-associated and environmental bacteria" by Chavez et al. 2021
[Link] to publication to come

Raw data is available on NCBI within the BioProject PRJNA678007

### Prerequirement ###
You will need:

* Data files present in the `data` folder
* an environement dedicated to qiime2-2020.6
* R
 * vegan
 * labdsv
 * tidyverse
 * reshape2
 * ggpubr
 * RColorBrewer
 * labdsv
* the Ancom Script from this [package](https://github.com/FrederickHuangLin/ANCOM/blob/master/scripts/ancom_v2.1.R)


### Processing Raw Reads with Qiime2 ###

* Script

### Processing Biomfile for statistical analysis ###

* [Script](https://bitbucket.org/the-samuel-lab/chavez-2020/src/master/satistical.analysis.Rmd)
* Report [[html](http://htmlpreview.github.io/?https://bitbucket.org/the-samuel-lab/chavez-2020/raw/86f418981dff6afeadf1423abfb202352d0fe1d0/statistics.report.html) - [pdf](https://bitbucket.org/the-samuel-lab/chavez-2020/raw/f4ef966d0c73dc6c3db082802fc45b469197adf2/statistics.report.pdf)]